def aTokkenSplit(aPatternToParse):
    '''
    :param aPatternToParse: string - The pattern as string
    :return: dict - the pattern as a dictionary with an integer as key to represent the position and list in value to represent the possible letter at this position
    '''
    aPatternToken = {}
    aCurrentTokenIndex = 0
    aTempGroupBuffer = []
    aExploringGroup = False
    for aOneLetter in aPatternToParse:
        if aOneLetter == '(':
            # begining of a repetable ground
            aExploringGroup = True
        elif aOneLetter == ')':
            aExploringGroup = False
            aPatternToken[aCurrentTokenIndex] = aTempGroupBuffer
            aTempGroupBuffer = []
            aCurrentTokenIndex = aCurrentTokenIndex + 1
        else:
            # normal char...
            if (aExploringGroup):
                aTempGroupBuffer.append(aOneLetter)
            else:
                aPatternToken[aCurrentTokenIndex] = [aOneLetter]
                aCurrentTokenIndex = aCurrentTokenIndex + 1
    return aPatternToken


def aWordMatch(aWord, aPatternToParse):
    '''
    :param aWord: string - Word to check against pattern
    :param aPatternToParse: string - Pattern (kind of REGEX)
    :return: bool - True if the word match the pattern
    '''
    aIdx=0
    aMatchPattern=True
    while aIdx < len(aWord) and aMatchPattern:
        if aWord[aIdx] not in aPatternToParse[aIdx]:
            aMatchPattern=False
        aIdx += 1
    return aMatchPattern


def aCounter(aDict, aPatternToParse):
    '''
    :param aDict: list - the list of string/word we are going to check against the pattern
    :param aPatternToParse: string - the pattern to check as a string
    :return: int - the number of word that match the pattern
    '''
    aNbMatch = 0
    aTokkens = aTokkenSplit(aPatternToParse)
    for aOneWord in aDict:
        if aWordMatch(aOneWord, aTokkens):
            aNbMatch += 1
    return aNbMatch


# Test case for aTokkenSplit
print(str(aTokkenSplit("(ab)(bc)(ca)")) + " should output: " + str({0: ["a", "b"], 1: ["b", "c"], 2: ["c", "a"]}))
print(str(aTokkenSplit("abc")) + " should output: " + str({0: ["a"], 1: ["b"], 2: ["c"]}))
print(str(aTokkenSplit("(abc)(abc)(abc)")) + " should output: " + str({0: ["a", "b", "c"], 1: ["a", "b", "c"], 2: ["a", "b", "c"]}))
print(str(aTokkenSplit("(zyx)bc")) + " should output: " + str({0: ["z", "y", "x"], 1: ["b"], 2: ["c"]}))

# Test case for aCounter
print(str(aCounter(["abc", "bca", "dac", "dbc", "cba"], "(ab)(bc)(ca)")) + " should output 2")
print(str(aCounter(["abc", "bca", "dac", "dbc", "cba"], "abc")) + " should output 1")
print(str(aCounter(["abc", "bca", "dac", "dbc", "cba"], "(abc)(abc)(abc)")) + " should output 3")
print(str(aCounter(["abc", "bca", "dac", "dbc", "cba"], "(zyx)bc")) + " should output 0")